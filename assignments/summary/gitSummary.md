# Git Basics

[![N|Solid](https://cdn.vox-cdn.com/thumbor/WO_mw7h7CbqFpzHI4OZqVYpQecA=/0x0:2040x1360/920x613/filters:focal(1287x538:1613x864):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/63739082/git.0.jpg)](https://git-scm.com/)

### What is Git?
  - Git is version-control software that makes collaboration with teammates super simple.
  - Git lets you easily keep track of every revision you and your team make during the development of your software.
  - The raw performance characteristics of Git are very strong when compared to many alternatives. Committing new changes, branching, merging and comparing past versions are all optimized for performance. 

### Git Terminologies
- [Repository](https://www.geeksforgeeks.org/what-is-a-git-repository/)
- [Gitlab](https://about.gitlab.com/what-is-gitlab/)
- [Commit](https://www.git-tower.com/learn/git/commands/git-commit/#:~:text=The%20%22commit%22%20command%20is%20used,changes%20to%20the%20local%20repository.&text=Using%20the%20%22git%20commit%22%20command,%22git%20push%22%20commands)
- [Push](https://www.atlassian.com/git/tutorials/syncing/git-push#:~:text=The%20git%20push%20command%20is,repository%20to%20a%20remote%20repo.&text=Remote%20branches%20are%20configured%20using,should%20be%20taken%20when%20pushing.)
- [Branch](https://www.atlassian.com/git/tutorials/using-branches#:~:text=Git%20branches%20are%20effectively%20a%20pointer%20to%20a%20snapshot%20of%20your%20changes.&text=Instead%20of%20copying%20files%20from,not%20a%20container%20for%20commits.)
- [Merge](https://www.atlassian.com/git/tutorials/using-branches/git-merge#:~:text=Merging%20is%20Git's%20way%20of,merge%20into%20the%20current%20branch.)
- [Clone](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone)
- [Fork](https://guides.github.com/activities/forking/#:~:text=This%20process%20is%20known%20as,up%20to%20the%20original%20project.)

### Git Workflow
[![N|Solid](https://nexuslinkservices.com/wp-content/uploads/2020/01/Git.jpg)](https://nexuslinkservices.com/git-workflow-in-software-development-life-cycle/)


    
### Basic Docker Commands

|   Command        | Description      | 
| -------------   |:-------------:| 
| `$ git init`   | This command turns a directory into an empty Git repository. | 
| `$ git add`| Adds files in the to the staging area for Git.     |
| `$ git commit` | Record the changes made to the files to a local repository. For easy reference, each commit has a unique ID.    |
| `$ git status`  | This command returns the current state of the repository.     | 
| `$ git config`   | With Git, there are many configurations and settings possible.This command assigns settings   | 

For more commands refer [this](http://guides.beanstalkapp.com/version-control/common-git-commands.html)









