# Docker Basics

[![N|Solid](https://images.xenonstack.com/blog/docker-overview-architecture-application-deployment.png)](https://www.docker.com/)

### What is Docker?
  - Docker is an open source software
  - Docker is a tool designed to make it easier to create, deploy, and run applications by using containers.
  - Docker allows applications to use the same Linux kernel as the system that they're running on. 

### Docker Terminologies
- [Container](https://www.docker.com/resources/what-container)
- [Docker Images](https://www.tutorialspoint.com/docker/docker_images.htm)
- [Docker Hub](https://www.docker.com/products/docker-hub#:~:text=Docker%20Hub%20is%20a%20hosted,push%20them%20to%20Docker%20Hub)
- [Docker File](https://docs.docker.com/engine/reference/builder/#:~:text=A%20Dockerfile%20is%20a%20text,can%20use%20in%20a%20Dockerfile%20.)
- [Registry](https://docs.docker.com/registry/introduction/#:~:text=A%20registry%20is%20a%20storage,available%20in%20different%20tagged%20versions.&text=Users%20interact%20with%20a%20registry,itself%20is%20delegated%20to%20drivers.)

### Basic Docker Architecture
[![N|Solid](https://www.whizlabs.com/blog/wp-content/uploads/2019/08/Docker_Architecture.png)](https://www.docker.com/)

### Installing Docker On Ubuntu 18.04
    $ sudo apt-get update
    $ sudo apt-get install \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg-agent \
      software-properties-common
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    $ sudo apt-key fingerprint 0EBFCD88
    $ sudo add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
      stable nightly test"
    $ sudo apt-get update
    $ sudo apt-get install docker-ce docker-ce-cli containerd.io

     // Check if docker is successfully installed in your system
    $ sudo docker run hello-world
    
### Basic Docker Commands

|   Command        | Description      | 
| -------------   |:-------------:| 
| `$ docker ps`   | Allows us to view all the containers that are running on the Docker Host | 
| `$ docker start`| This command starts any stopped container(s)     |
| `$ docker stop` | This command stops any running container(s)      |
| `$ docker run`  | This command creates containers from docker images      | 
| `$ docker rm`   | This command deletes the containers    | 


### Basic Opeations in Docker
1. Download/pull the docker images that you want to work with.
2. Copy your code inside the docker
3. Access docker terminal
4. Install and additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run your program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try your code.

You can refer [this](https://docs.docker.com/) for detailed explaination.






